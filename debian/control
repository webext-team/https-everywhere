Source: https-everywhere
Section: web
Priority: optional
Maintainer: Debian Mozilla Extension Maintainers <pkg-mozext-maintainers@lists.alioth.debian.org>
Uploaders:
 Fabrizio Regalli <fabreg@fabreg.it>,
 Jérémy Bobbio <lunar@debian.org>,
 Markus Koschany <apo@debian.org>,
 Michael Meskes <meskes@debian.org>
Build-Depends:
 debhelper-compat (= 13)
Standards-Version: 4.6.1
Homepage: https://www.eff.org/https-everywhere
Vcs-Git: https://salsa.debian.org/webext-team/https-everywhere.git
Vcs-Browser: https://salsa.debian.org/webext-team/https-everywhere

Package: webext-https-everywhere
Architecture: all
Depends:
 ${misc:Depends},
Recommends:
 firefox (>= 50) | firefox-esr (>= 50) | chromium
Description: Extension to force the use of HTTPS on many sites
 HTTPS Everywhere is a Firefox/Iceweasel/Chromium extension produced as a
 collaboration between The Tor Project and the Electronic Frontier
 Foundation. It encrypts your communications with a number of major
 websites.
 .
 Many sites on the web offer some limited support for encryption over HTTPS,
 but make it difficult to use. For instance, they may default to unencrypted
 HTTP, or fill encrypted pages with links that go back to the unencrypted
 site.
 .
 The HTTPS Everywhere extension fixes these problems by rewriting all
 requests to these sites to HTTPS.
