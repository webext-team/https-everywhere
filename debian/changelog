https-everywhere (2022.5.11-1) unstable; urgency=medium

  * New upstream version 2022.5.11.
  * Declare compliance with Debian Policy 4.6.1.

 -- Markus Koschany <apo@debian.org>  Sat, 28 May 2022 13:57:07 +0200

https-everywhere (2021.7.13-1) unstable; urgency=medium

  * New upstream version 2021.7.13.
  * Declare compliance with Debian Policy 4.6.0.

 -- Markus Koschany <apo@debian.org>  Wed, 18 Aug 2021 20:33:25 +0200

https-everywhere (2021.4.15-1) experimental; urgency=medium

  * New upstream version 2021.4.15.

 -- Markus Koschany <apo@debian.org>  Sun, 18 Apr 2021 00:22:29 +0200

https-everywhere (2021.1.27-1) unstable; urgency=medium

  * New upstream version 2021.1.27.

 -- Markus Koschany <apo@debian.org>  Sat, 30 Jan 2021 23:59:40 +0100

https-everywhere (2020.11.17-1) unstable; urgency=medium

  * New upstream version 2020.11.17.
  * Declare compliance with Debian Policy 4.5.1.

 -- Markus Koschany <apo@debian.org>  Wed, 18 Nov 2020 15:34:29 +0100

https-everywhere (2020.8.13-1) unstable; urgency=medium

  * New upstream version 2020.8.13.

 -- Markus Koschany <apo@debian.org>  Tue, 18 Aug 2020 01:04:48 +0200

https-everywhere (2020.5.20-1) unstable; urgency=medium

  * New upstream version 2020.5.20.
  * Switch to debhelper-compat = 13.

 -- Markus Koschany <apo@debian.org>  Thu, 28 May 2020 22:19:06 +0200

https-everywhere (2020.3.16-1) unstable; urgency=medium

  * New upstream version 2020.3.16.
  * Declare compliance with Debian Policy 4.5.0.

 -- Markus Koschany <apo@debian.org>  Sat, 21 Mar 2020 00:29:05 +0100

https-everywhere (2019.11.7-1) unstable; urgency=medium

  * New upstream version 2019.11.7.
  * Declare compliance with Debian Policy 4.4.1.
  * Drop transitional xul-ext-https-everywhere package. (Closes: #939242)
  * Add missing sources for wasm library to debian/missing-sources.

 -- Markus Koschany <apo@debian.org>  Sun, 17 Nov 2019 02:01:29 +0100

https-everywhere (2019.6.27-2) unstable; urgency=medium

  * Install the wasm directory too.
    Thanks to Martin Steigerwald for the report. (Closes: #932721)

 -- Markus Koschany <apo@debian.org>  Mon, 22 Jul 2019 16:15:15 +0200

https-everywhere (2019.6.27-1) unstable; urgency=medium

  * New upstream version 2019.6.27.
  * Use debhelper-compat = 12.
  * Declare compliance with Debian Policy 4.4.0.

 -- Markus Koschany <apo@debian.org>  Thu, 18 Jul 2019 05:03:16 +0200

https-everywhere (2019.1.31-2) unstable; urgency=medium

  [ Michael Meskes ]
  * With chromium now loading all extensions we do not need to provide a
    solution ourselves.

  [ Markus Koschany ]
  * Recommend firefox || firefox-esr || chromium instead of depending on them.
    A dependency will prevent migrations to testing if firefox is affected by RC
    bugs. Firefox or Chromium addons work perfectly fine even with current
    versions in testing. Besides this would be in line with other addons like
    ublock-origin or privacybadger.

 -- Markus Koschany <apo@debian.org>  Wed, 13 Feb 2019 17:57:24 +0100

https-everywhere (2019.1.31-1) unstable; urgency=medium

  * New upstream version 2019.1.31.

 -- Markus Koschany <apo@debian.org>  Tue, 05 Feb 2019 14:32:29 +0100

https-everywhere (2019.1.7-1) unstable; urgency=medium

  * New upstream version 2019.1.7.
  * Declare compliance with Debian Policy 4.3.0.

 -- Markus Koschany <apo@debian.org>  Sat, 12 Jan 2019 15:06:37 +0100

https-everywhere (2018.10.31-1) unstable; urgency=medium

  * New upstream version 2018.10.31.

 -- Markus Koschany <apo@debian.org>  Thu, 01 Nov 2018 13:40:41 +0100

https-everywhere (2018.9.19-1) unstable; urgency=medium

  * New upstream version 2018.9.19.
  * Declare compliance with Debian Policy 4.2.1.

 -- Markus Koschany <apo@debian.org>  Sat, 22 Sep 2018 11:39:06 +0200

https-everywhere (2018.8.22-1) unstable; urgency=medium

  * New upstream version 2018.8.22.
  * Add missing codemirror sources.

 -- Markus Koschany <apo@debian.org>  Sat, 25 Aug 2018 18:27:48 +0200

https-everywhere (2018.6.21-1) unstable; urgency=medium

  * New upstream version 2018.6.21.
  * Add myself to Uploaders.
  * Declare compliance with Debian Policy 4.2.0.
  * Drop gbp.conf.
  * Update README.source and explain how to fetch upstream releases.
  * Drop debian/docs because README.Debian is installed automatically.

 -- Markus Koschany <apo@debian.org>  Fri, 03 Aug 2018 08:48:52 +0200

https-everywhere (2018.4.11-1) unstable; urgency=medium

  * New upstream version 2018.4.11

 -- Michael Meskes <meskes@debian.org>  Fri, 13 Apr 2018 16:41:45 +0200

https-everywhere (2018.4.3-1) unstable; urgency=medium

  * New upstream version 2018.4.3
  * Bumped standards version.

 -- Michael Meskes <meskes@debian.org>  Sun, 08 Apr 2018 11:33:04 +0200

https-everywhere (2018.3.13-1) unstable; urgency=medium

  * New upstream version 2018.3.13
  * Make sure obsolete config file gets deleted. (Closes: #893468)

 -- Michael Meskes <meskes@debian.org>  Mon, 19 Mar 2018 20:10:03 +0100

https-everywhere (2018.2.26-1) unstable; urgency=medium

  * New upstream version 2018.2.26

 -- Michael Meskes <meskes@debian.org>  Tue, 06 Mar 2018 21:27:35 +0100

https-everywhere (2018.1.29-2) unstable; urgency=medium

  * Added firefox-esr as alternative recommendation (Closes: #890727)
  * Add file to load all extension into chromium

 -- Michael Meskes <meskes@debian.org>  Sun, 18 Feb 2018 12:31:28 +0100

https-everywhere (2018.1.29-1) unstable; urgency=medium

  * Changed gbp config to use new section naming.
  * New upstream version 2018.1.29 (Closes: #881137)
  * Added myself as uploader
  * Bumped standards and debhelper version.
  * Changed packaging to building one web extension usable by both,
    chromium and firefox.
  * Moved to salsa.
  * Document how to enable the extension in chromium.
  * Removed old build dependencies that don't seem to be needed anymore.

 -- Michael Meskes <meskes@debian.org>  Wed, 14 Feb 2018 11:08:03 +0100

https-everywhere (5.2.8-1) unstable; urgency=medium

  * New upstream release.

 -- Jérémy Bobbio <lunar@debian.org>  Thu, 01 Dec 2016 10:42:54 +0100

https-everywhere (5.2.7-1) unstable; urgency=medium

  * New upstream release.

 -- Jérémy Bobbio <lunar@debian.org>  Wed, 30 Nov 2016 17:28:34 +0100

https-everywhere (5.2.6-1) unstable; urgency=medium

  * New upstream release.
  * Add missing step in debian/README.source.

 -- Jérémy Bobbio <lunar@debian.org>  Thu, 27 Oct 2016 12:49:52 +0200

https-everywhere (5.2.4-1) unstable; urgency=medium

  * New upstream release. (Closes: #836125, #801092, #819968)
  * Refresh and delete obsolete patches. (Closes: #810388)
  * Ship translations as an additional upstream tarball to match the new
    git submodule structure. Update relevant instructions in
    debian/README.source.
  * Update generated XPI archive name in debian/rules.
  * Bump Standards-Version, no changes required.
  * Switch Vcs-* URLs to HTTPS.

 -- Jérémy Bobbio <lunar@debian.org>  Sun, 04 Sep 2016 18:22:48 +0200

https-everywhere (5.1.1-2) unstable; urgency=medium

  * Team upload.
  * Drop locales-all from Build-Depends. It is not needed any more.
  * Update debian/copyright
  * Switch to debhelper 9

 -- Benjamin Drung <bdrung@debian.org>  Sun, 18 Oct 2015 00:40:02 +0200

https-everywhere (5.1.1-1) unstable; urgency=medium

  * New upstream release. (Closes: #798325)
  * Fix watch file.
  * Remove recording of upstream Git commit Id as the “display source”
    feature is gone.
  * Refresh patches.
  * Add rsync to the Build-Depends, now used by the makexpi.sh script.
  * Fix XPI name in debian/rules.

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 03 Oct 2015 20:39:27 +0200

https-everywhere (5.0.7-1) unstable; urgency=medium

  * New upstream release.

 -- Jérémy Bobbio <lunar@debian.org>  Sun, 19 Jul 2015 11:25:03 +0200

https-everywhere (5.0.6-1) unstable; urgency=medium

  * New upstream release.

 -- Jérémy Bobbio <lunar@debian.org>  Tue, 14 Jul 2015 20:41:45 +0200

https-everywhere (5.0.5-1) unstable; urgency=medium

  * New upstream release.

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 30 May 2015 12:26:50 +0200

https-everywhere (5.0.4-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches:
    - Remove 03-update-debian-rules.diff, merged upstream.
    - Update the rest.

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 16 May 2015 11:36:41 +0200

https-everywhere (4.0.3-1) unstable; urgency=low

  * New upstream release. (Closes: #779733)

 -- Jérémy Bobbio <lunar@debian.org>  Thu, 19 Mar 2015 11:42:33 +0100

https-everywhere (4.0.2-3) unstable; urgency=medium

  * forgotten bug closer in previous changelog entry
  * add Bug-Debian to no-unconditional-FAQ-load.patch description

 -- Damyan Ivanov <dmn@debian.org>  Fri, 28 Nov 2014 09:59:35 +0000

https-everywhere (4.0.2-2) unstable; urgency=medium

  * Team upload

  * Patch src/chrome/content/toolbar_button.js to make loading of the FAQ
    require user action.
    When run for the first time, the addon shows a notification bar.
    Closing that bar loads the HTTPS-everywhere FAQ from the authors' site.
    This is a privacy breach. This patch requires pressing a 'FAQ…' button
    before loading the remote page.
    Closes: #771286

 -- Damyan Ivanov <dmn@debian.org>  Fri, 28 Nov 2014 09:31:54 +0000

https-everywhere (4.0.2-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version, no changes required.

 -- Jérémy Bobbio <lunar@debian.org>  Fri, 17 Oct 2014 00:14:08 +0200

https-everywhere (4.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Build-Depends on locales-all as make-sqlite.py requires en_US.UTF-8.

 -- Jérémy Bobbio <lunar@debian.org>  Fri, 12 Sep 2014 05:29:51 +0000

https-everywhere (4.0.0-1) unstable; urgency=low

  * New upstream release.
  * Update patch with Debian specific rules. (Closes: #757234)

 -- Jérémy Bobbio <lunar@debian.org>  Wed, 03 Sep 2014 02:07:53 +0000

https-everywhere (3.5.3-1) unstable; urgency=medium

  * New upstream release.
  * Remove obsolete patch now that using a system-wide ruleset works again.

 -- Jérémy Bobbio <lunar@debian.org>  Fri, 27 Jun 2014 10:14:39 +0200

https-everywhere (3.5.1-1) unstable; urgency=low

  * New upstream release:
    - Re-enable ability to see all rulesets in enable/disable dialog.
      (Closes: #744975)
    - Fix XKCD rule. (Closes: #745131)
  * Drop obsolete patches for Debian rules.

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 26 Apr 2014 08:00:40 +0200

https-everywhere (3.5-1) unstable; urgency=low

  * New upstream version.
  * Update Build-Depends.
  * Disable rules that depend on CACert.org certificates. (Closes: #744230)
  * Update Debian rules from master branch and Peter Palfrader's patches.
    (Closes: #738539)
  * Drop obsolete patch bumping MaxVersion.
  * Refresh use-newer-timestamp-date.patch.
  * Record upstream Git commit id when building source.
  * Add a patch to properly load the rulesets database.

 -- Jérémy Bobbio <lunar@debian.org>  Tue, 15 Apr 2014 19:21:38 +0200

https-everywhere (3.4.5-1) unstable; urgency=low

  * New upstream release.
  * Update debian/copyright.

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 04 Jan 2014 10:29:21 +0100

https-everywhere (3.4.3-2) unstable; urgency=medium

  * Team upload.
  * Add use-newer-timestamp-date.patch to use a recent date for the timestamp
    of the files instead of using 1980-01-01.
  * Add make-clean.patch to remove all auto-generated files on clean.
  * Bump Standards-Version to 3.9.5 (no changes required).

 -- Benjamin Drung <bdrung@debian.org>  Sat, 21 Dec 2013 20:40:16 +0100

https-everywhere (3.4.3-1) unstable; urgency=low

  * New upstream release.

 -- Jérémy Bobbio <lunar@debian.org>  Thu, 05 Dec 2013 14:39:42 +0100

https-everywhere (3.4.2-1) unstable; urgency=low

  * Team upload
  * New upstream release

 -- David Prévot <taffit@debian.org>  Mon, 21 Oct 2013 14:34:15 -0400

https-everywhere (3.4.1-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches against new upstream version.

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 17 Aug 2013 23:31:35 +0200

https-everywhere (3.3.1-1) unstable; urgency=low

  * New upstream release.
  * Update 01-enable-cacert-rules.diff against new upstream version.

 -- Jérémy Bobbio <lunar@debian.org>  Tue, 06 Aug 2013 00:39:09 +0200

https-everywhere (3.2.4-1) unstable; urgency=low

  * Team upload
  * Use canonical Vcs- fields
  * Drop now useless Pre-Depends: dpkg 1.15.8.13 is in squeeze
  * Update copyright

 -- David Prévot <taffit@debian.org>  Mon, 15 Jul 2013 19:51:49 -0400

https-everywhere (3.2.2-1) unstable; urgency=low

  * New upstream release.

 -- Jérémy Bobbio <lunar@debian.org>  Thu, 23 May 2013 10:17:21 +0200

https-everywhere (3.2.1-1) unstable; urgency=low

  * New upstream release.
  * Remove obsolete DM-Upload-Allowed field.

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 18 May 2013 08:50:50 +0200

https-everywhere (3.2-2) unstable; urgency=low

  * Upload to unstable.

 -- Jérémy Bobbio <lunar@debian.org>  Sun, 05 May 2013 20:24:48 +0200

https-everywhere (3.2-1) experimental; urgency=low

  * New upstream release.
  * Use snapshot from Git upstream repository as source in order to work from
    the preferred form of modifications:
    - Adapt debian/rules to build from upstream Git repository.
    - Add python-lxml and libxml2-utils to Build-Depends.
    - Adapt patches to the new source.
    - Configure git-buildpackage to commit first upstream snapshot using
      pristine-tar.
    - Update README.source.
  * Make use of ${xpi:Depends} as recommended by mozilla-devscripts.
  * Bump Standards-Version, no changes required.

 -- Jérémy Bobbio <lunar@debian.org>  Sun, 28 Apr 2013 11:54:16 +0000

https-everywhere (3.1.4-1) experimental; urgency=low

  * Imported Upstream version 3.1.4
  * Refresh debian/patches/03-enable-debian-rules.diff

 -- Jérémy Bobbio <lunar@debian.org>  Fri, 08 Mar 2013 11:45:41 +0100

https-everywhere (3.1.3-2) experimental; urgency=low

  * Add a ruleset for SSL DebConf domains. Thanks to Paul Wise for the initial
    patch. (Closes: #702396)

 -- Jérémy Bobbio <lunar@debian.org>  Wed, 06 Mar 2013 11:37:37 +0100

https-everywhere (3.1.3-1) experimental; urgency=low

  * Compress binary package using xz using the good enough level 2
  * Imported Upstream version 3.1.3
  * Refresh debian/patches/03-enable-debian-rules.diff

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 19 Jan 2013 11:56:02 +0100

https-everywhere (3.1.2-1) experimental; urgency=low

  * Imported Upstream version 3.1.2
  * Refresh debian/patches/03-enable-debian-rules.diff

 -- Jérémy Bobbio <lunar@debian.org>  Fri, 04 Jan 2013 10:50:35 +0100

https-everywhere (3.1-1) experimental; urgency=low

  * Imported Upstream version 3.1
  * Refresh debian/patches/03-enable-debian-rules.diff

 -- Jérémy Bobbio <lunar@debian.org>  Wed, 12 Dec 2012 18:24:37 +0100

https-everywhere (3.0.4-1) experimental; urgency=low

  * Imported Upstream version 3.0.4
  * Refresh debian/patches/03-enable-debian-rules.diff

 -- Jérémy Bobbio <lunar@debian.org>  Mon, 12 Nov 2012 11:02:56 +0100

https-everywhere (3.0.3-1) experimental; urgency=low

  * Imported Upstream version 3.0.3
  * Refreshed 03-enable-debian-rules patch

 -- Jérémy Bobbio <lunar@debian.org>  Tue, 30 Oct 2012 20:05:25 +0100

https-everywhere (3.0.2-1) experimental; urgency=low

  * Imported Upstrema version 3.0.2 (Closes: #687856)
  * Split 01-enable-rules-disabled patch into 01-enable-cacert-rules
    and 03-enable-debian-rules
  * 01-enable-cacert-rules enables rules for certificates issued by CACert
    through the new platform mechanism
  * Update debian/copyright
  * Add myself to Uploaders

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 20 Oct 2012 12:32:13 +0200

https-everywhere (2.0.5-1) unstable; urgency=low

  * Imported Upstream version 2.0.5
  * Refreshed 01-enable-rules-disabled patch

 -- Fabrizio Regalli <fabreg@fabreg.it>  Wed, 23 May 2012 11:55:04 +0200

https-everywhere (2.0.3-1) unstable; urgency=low

  * Imported Upstream version 2.0.3
  * Refreshed 01-enable-rules-disabled patch

 -- Fabrizio Regalli <fabreg@fabreg.it>  Sun, 29 Apr 2012 10:30:34 +0200

https-everywhere (2.0.2-1) unstable; urgency=low

  * Imported Upstream version 2.0.2
  * Refreshed 01-enable-rules-disabled patch

 -- Fabrizio Regalli <fabreg@fabreg.it>  Wed, 25 Apr 2012 14:28:50 +0200

https-everywhere (2.0.1-2) unstable; urgency=low

  * Re-introduced 02-bump-icedove-maxversion.diff patch:
    it's needed for Icedove yet

 -- Fabrizio Regalli <fabreg@fabreg.it>  Tue, 28 Feb 2012 16:59:24 +0100

https-everywhere (2.0.1-1) unstable; urgency=low

  * Imported Upstream version 2.0.1 (Closes: #661588)
  * Removed no more necessary Breaks: ${xpi:Breaks} in d/control
  * Bump Standards-Version to 3.9.3
  * Update format to copyright-format 1.0
  * Added myself to Copyright
  * Removed no more necessary 02-bump-icedove-maxversion.diff patch
  * Refreshed 01-enable-rules-disabled patch

 -- Fabrizio Regalli <fabreg@fabreg.it>  Tue, 28 Feb 2012 11:56:18 +0100

https-everywhere (1.2.2-1) unstable; urgency=low

  * Imported Upstream version 1.2.2 (Closes: #656648)

 -- Fabrizio Regalli <fabreg@fabreg.it>  Fri, 20 Jan 2012 21:24:58 +0100

https-everywhere (1.2.1-2) unstable; urgency=low

  * Added 02-bump-icedove-maxversion.diff patch for introducing
    compatibility with all Icedove versions (Closes: #654661)

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 05 Jan 2012 15:21:25 +0100

https-everywhere (1.2.1-1) unstable; urgency=low

  * Imported Upstream version 1.2.1
  * Added Pre-Depends: dpkg (>= 1.15.6) to d/control
  * Refreshed 01-enable-rules-disabled.diff patch

 -- Fabrizio Regalli <fabreg@fabreg.it>  Sun, 04 Dec 2011 23:08:01 +0100

https-everywhere (1.2-2) unstable; urgency=low

  * Added Breaks: ${xpi:Breaks} to d/control (Closes: #649025)

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 17 Nov 2011 09:52:57 +0100

https-everywhere (1.2-1) unstable; urgency=low

  * Uploading to unstable
  * Imported Upstream version 1.2
  * Refreshed 01-enable-rules-disabled.diff patch for unstable.

 -- Fabrizio Regalli <fabreg@fabreg.it>  Wed, 16 Nov 2011 21:16:23 +0100

https-everywhere (2.0~development.3-1) experimental; urgency=low

  * Imported Upstream version 2.0development.3 (Closes: #642376)
  * Changing 01-enable-rules-disabled.diff patch for experimental.

 -- Fabrizio Regalli <fabreg@fabreg.it>  Sat, 05 Nov 2011 12:34:47 +0100

https-everywhere (1.1-1) unstable; urgency=low

  * Fixed d/watch
  * Imported Upstream version 1.1
  * Refreshed 01-enable-rules-disabled.diff patch

 -- Fabrizio Regalli <fabreg@fabreg.it>  Sat, 05 Nov 2011 12:10:29 +0100

https-everywhere (1.0.3-2) unstable; urgency=low

  * Added 01-enable-rules-disabled.diff patch (Closes: #638775)
    Thanks to Vagrant Cascadian for patch.

 -- Fabrizio Regalli <fabreg@fabreg.it>  Fri, 04 Nov 2011 09:31:35 +0100

https-everywhere (1.0.3-1) unstable; urgency=low

  [ Rogério Brito ]
  * debian/rules: Use xz instead of bzip2. Thanks to Paul Wise.
  * debian/rules: Enable parallel building. Thanks Paul Wise.

  [ Fabrizio Regalli ]
  * Imported Upstream version 1.0.3 (Closes: #643731)
  * Added myself to Uploaders
  * Added DM upload flag
  * Fixed unversioned-copyright-format-uri lintian message
  * Removed no necessary extra license file

 -- Fabrizio Regalli <fabreg@fabreg.it>  Sat, 08 Oct 2011 14:52:44 +0200

https-everywhere (1.0.2-1) unstable; urgency=low

  * New upstream release.
  * Mention pristine-tar in debian/README.source.
  * debian/README.source: Mention pristine-tar.
  * debian/gbp.conf: Be more specific about the branches used.

 -- Rogério Brito <rbrito@ime.usp.br>  Thu, 22 Sep 2011 12:56:47 -0300

https-everywhere (1.0.1-1) unstable; urgency=low

  * New upstream version.
  * debian/control: Remove arch-dependent shlibs substitution.
  * debian/README.source: Update the instructions with the final URL.
  * Add minimal gbp.conf configuration.

 -- Rogério Brito <rbrito@ime.usp.br>  Wed, 17 Aug 2011 21:57:56 -0300

https-everywhere (1.0.0-1) unstable; urgency=low

  * Initial release for Debian. Closes: #591579.
  * Imported Upstream version 1.0.0
  * debian/control:
    + Set the maintainer to be the mozext team.
    + We now comply to policy version 3.9.2.
    + Add the future URLs where the git repository will be.
    + Use wrap-and-sort to normalize the fields.
    + Make build-dependency on mozilla-devscripts unversioned.
  * debian/README.source:
    + Add a README.source file describing how the package is usually made.
  * debian/watch:
    + Fix it to grab only released versions, not development ones.

 -- Rogério Brito <rbrito@ime.usp.br>  Mon, 08 Aug 2011 17:56:34 -0300

https-everywhere (0.9.9.development.4-1) unstable; urgency=low

  * New upstream release

 -- Rogério Brito <rbrito@ime.usp.br>  Mon, 14 Mar 2011 02:38:49 -0300

https-everywhere (0.9.9.development.4~git20110310+6331e2-1~ppa0) maverick; urgency=low

  * Update to git revision 6331e2 from master branch.
  * debian/changelog:
    + Adopt a new versioning scheme. Not nice, but seems to work.
  * Upload to my ppa.

 -- Rogério Brito <rbrito@ime.usp.br>  Thu, 10 Mar 2011 21:22:27 -0300

https-everywhere (0.9.9.development.4~b66ad4-1~ppa0) maverick; urgency=low

  * Update to git revision b66ad4 from master branch.
  * debian/copyright:
    + Rewrite from scratch with proper attribution in DEP-5 format.
  * debian/{compat,control}:
    + Update the debhelper compatibility version to 8.
  * debian/control:
    + Include wording that the package is also meant for iceweasel.
  * debian/rules:
    + Use the proper way of passing arguments to the dh command.
  * Upload to my ppa.

 -- Rogério Brito <rbrito@ime.usp.br>  Fri, 11 Feb 2011 11:50:23 -0200

https-everywhere (0.9.9.development.3~pre-1~ppa0) maverick; urgency=low

  * New upstream snapshot, with many extra rules.
  * Upload to my ppa.

 -- Rogério Brito <rbrito@ime.usp.br>  Thu, 20 Jan 2011 17:11:18 -0200

https-everywhere (0.9.9.development.2-1~ppa0) maverick; urgency=low

  * Initial release targetting my ppa.

 -- Rogério Brito <rbrito@ime.usp.br>  Tue, 11 Jan 2011 00:40:22 -0200
